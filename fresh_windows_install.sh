# Chocolatey install script from earlier

powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"


# Install all the packages
# -y confirm yes for any prompt during the install process ﻿

choco install googlechrome -y
choco install microsoft-windows-terminal -y 
choco install neovim --pre -y
choco install docker-desktop -y
choco install vscode -y
choco install wsl-ubuntu-2004 -y


# choco install <package_name> repeats for all the packages you want to install
