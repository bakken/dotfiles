#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   	echo "This script must be run as root"
   	exit 1
else
	cmd=(dialog --separate-output --checklist "Please Select Software you want to install:" 22 76 16)
	options=(0 "Update, upgrade and install dialog, wget, curl ++" off # any option can be set to default to "on"
		1 "Neovim" off
		2 "Git" off
		3 "Chrome" off
		4 "Oh-My-Zsh" off
		5 "Docker" off
		6 "i3" off
		7 "i3 post install" off
		8 "Neovim python env" off
		9 "Create symlinks" off
		10 "Fonts" off
		11 "Fzf" off
		12 "Zsh gruvbox color scheme" off
        13 "Arandr (display settings GUI)" off
        14 "Div utility programs" off
        15 "Pyql" off
        16 "Insync" off
        17 "VS Code" off
        18 "Zathura and Okular" off
        19 "Pandoc and Latex" off
        20 "Blueman" off
        21 "Postgresql" off
        22 "Taskwarrior" off
        23 "Poppler Utils" off
	)

	choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
	clear

	for choice in $choices
	do
		case $choice in
			0)
				echo -e "\e[33mUpdating and Upgrading \e[39m"
				apt --fix-broken install -y
				apt-get update && sudo apt-get upgrade -y
				sudo apt install apt-transport-https ca-certificates curl software-properties-common wget curl dialog -y
				;;

			1)
				echo -e "\e[33mInstalling Neovim \e[39m"
				curl -LO https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
				mkdir -p ~/.appimages
				mv nvim.appimage /home/bjorn/.appimages/
				chmod +x /home/bjorn/.appimages/nvim.appimage
				sudo rm -f /bin/nvim
				sudo ln -s /home/bjorn/.appimages/nvim.appimage /bin/nvim
				;;
			2)
				echo -e "\e[33mInstalling Git \e[39m"
				sudo apt-get install git -y
				;;
			3)
				echo -e "\e[33mInstalling Google Chrome \e[39m"
				wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
				sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
				apt-get -y update
				apt-get install google-chrome-stable -y
				;;
			4)
				echo -m "\e[33mInstalling Oh-My-Zsh \e[39m"
				sudo apt-get install -y zsh
				wget –no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O – | sh;
				sudo chsh -s /bin/zsh root;
				wget --no-check-certificate http://install.ohmyz.sh -O - | sh;
				;;
			5)
				echo -e "\e[33mInstalling Docker \e[39m"
				curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add
				sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
				sudo apt update
				sudo apt install docker-ce virtualbox jq -y
				sudo usermod -aG docker ${USER}

                VERSION=$(curl --silent https://api.github.com/repos/docker/compose/releases/latest | jq .name -r)


				sudo curl -L https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
				sudo chmod +x /usr/local/bin/docker-compose

                base=https://github.com/docker/machine/releases/download/v0.16.0 &&
                    curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
                    sudo install /tmp/docker-machine /usr/local/bin/docker-machine
				;;
			6)
				echo -e "\e[33mInstalling i3 \e[39m"
				sudo apt install i3 compton feh -y
				echo -e "\e[1m\e[31mRemember to run postinstall after first login!!\e[39m\e[0m"
				;;
			7)
				echo -e "\e[i3 post install (run after first login) \e[39m"
				echo -e "\e[33mNot implemented yet... \e[39m"
				;;
			8)
				echo -e "\e[33mCreating python env for Neovim \e[39m"
				apt-get install python3-venv -y
				python3 -m venv ~/nvim_venv
				~/nvim_venv/bin/pip3 install flake8 pyflakes pycodestyle neovim
				;;
			9)
				echo -e "\e[33mCreating symlinks \e[39m"
                #Neovim
				mkdir -p ~/.config/nvim
				rm -f ~/.config/nvim/init.vim
				sudo ln -s /home/bjorn/dotfiles/init.vim /home/bjorn/.config/nvim/init.vim
                #Zsh
                rm -f ~/.zshrc
                sudo ln -s /home/bjorn/dotfiles/.zshrc /home/bjorn/.zshrc
                #i3
                mkdir -p ~/.config/i3
                rm -f ~/.config/i3/config
                sudo ln -s /home/bjorn/dotfiles/i3_config /home/bjorn/.config/i3/config
                ;;
            10)
                echo -e "\e[33mClone and install Nerdfonts \e[39m"
                git clone https://github.com/ryanoasis/nerd-fonts.git
                nerd-fonts/install.sh
                echo -e "\e[33mClone and install Powerline fonts \e[39m"
                sudo apt-get install fonts-powerline -y
                ;;
            11)
                echo -e "\e[33mClone and instal Fzf \e[39m"
                git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
                ~/.fzf/install
                ;;
            12)
                echo -e "\e[33mDownload/clone and instal zsh color schemes \e[39m"
                curl -L https://raw.githubusercontent.com/sbugzu/gruvbox-zsh/master/gruvbox.zsh-theme > ~/.oh-my-zsh/custom/themes/gruvbox.zsh-theme
                git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
                ;;
            13)
                echo -e "\e[33mArandd (display GUI) \e[39m"
                sudo apt-get install arandr -y
                echo -e "\e[33mLink to screenlayouts \e[39m"
                sudo ln -s /home/bjorn/dotfiles/screenlayout/hh1.sh /home/bjorn/.screenlayout/hh1.sh
                sudo ln -s /home/bjorn/dotfiles/screenlayout/home.sh /home/bjorn/.screenlayout/home.sh
                sudo ln -s /home/bjorn/dotfiles/screenlayout/laptop-display.sh /home/bjorn/.screenlayout/laptop-display.sh
                ;;
            14)
                echo -e "\e[33mUtilities \e[39m"
                sudo apt install xclip -y
                curl -L https://github.com/sharkdp/bat/releases/download/v0.11.0/bat_0.11.0_amd64.deb > bat.deb
                sudo dpkg -i bat.deb
                rm bat.deb
                curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash
				sudo apt-get install nodejs -y
                ;;
            15)
                echo -e "\e[33mCreate venv with pyql \e[39m"
                sudo apt-get install -y software-properties-common
                add-apt-repository ppa:edd/misc -y
                add-apt-repository ppa:deadsnakes/ppa -y
                sudo apt-get update -y
                sudo apt-get install -y python3.7 python3.7-dev python3-pip \
                    python3.7.venv build-essential g++ autotools-dev \
                    libicu-dev libbz2-dev git libxslt1-dev zlib1g-dev \
                    libxml2-dev libboost-all-dev libquantlib0-dev \
                    libquantlib0-dev quantlib-examples
                /usr/bin/python3.7 -m venv ~/.venv
                ~/.venv/bin/pip install --upgrade pip setuptools
                ~/.venv/bin/pip install cython numpy

                mkdir -p ~/src && cd scr
                git clone https://github.com/enthought/pyql.git
                cd pyql
                ~/.venv/bin/python setup.py build_ext --inplace
                ~/.venv/bin/pip install .
                sudo chown -R ~/.venv
                ;;
            16)
                echo -e "\e[33mInsync \e[39m"
                sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
                echo "deb http://apt.insynchq.com/ubuntu bionic non-free contrib" >> /etc/apt/sources.list.d/insync.list
                sudo apt update
                sudo apt-get install insync -y
                ;;
            17)
                echo -e "\e[33mVS Code \e[39m"
                sudo apt install software-properties-common apt-transport-https wget -y
                wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
                sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y
                sudo apt update -y
                sudo apt install code code-insiders -y
                ;;
            18)
                echo -e "\e[33mInstalling Zathura \e[39m"
                sudo apt-get update -y
                sudo apt-get install -y zathura okular
                ;;

            19)
                echo -e "\e[33mPandoc and Latex \e[39m"
                sudo apt-get update -y
                #wget http://users.phys.psu.edu/%7Ecollins/software/latexmk-jcc/latexmk-445.zip
                #unzip latexmk*.zip
                #sudo cp latexmk/latexmk.pl /usr/local/bin
                #sudo mv /usr/local/bin/latexmk.pl /usr/local/bin/latexmk
				#sudo apt-get install -y libsynctex-dev
				#sudo apt-get install -y libgtk-3-dev
				#~/nvim_venv/bin/pip3 install --upgrade setuptools
                #~/nvim_venv/bin/pip3 install neovim-remote
                sudo apt-get install -y pandoc
                sudo apt-get install -y texlive-full

				;;
            20)
                echo -e "\e[33mBlueman (bluetooth manager) \e39m""]"
                sudo apt-get install blueman -y
                ;;
            21)
                echo -e "\e[33mPostgresql \e39m""]"
                sudo apt install postgresql postgresql-contrib -y
                ;;
            22)
                echo -e "\e[33mTaskwarrior \e39m""]"
                sudo apt install taskwarrior -y
                echo -e "\e[33mBugwarrior \e39m""]"
                sudo apt install python-pip -y
                pip install "bugwarrior['gmail']"
                pip install "bugwarrior['jira']"
                sudo ln -s /home/bjorn/dotfiles/.taskrc /home/bjorn/.taskrc
                echo -e "\e[33mTaskopen \e39m""]"
                git clone https://github.com/ValiValpas/taskopen.git

                ;;
            23)
                echo -e "\e[33mPopplerutils \e39m""]"
                sudo apt install poppler-utils -y

                ;;
		esac
		echo -e "\nTake a \xF0\x9f\x8d\xba and restart (to be on the safe side)"
	done
fi

# Diverse:
# Node (install with nvm to avoid access issues):
#git clone https://github.com/nvm-sh/nvm.git ~/.nvm
#git checkout v0.34.0
#source ~/.nvm/nvm.sh
#nvm use node
#npm install -g parcel-bundler

