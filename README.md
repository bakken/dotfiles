# Fresh install on windows

## Get Git

Install Git "manually" by following [this link](https://git-scm.com/download/win)

## Set up an SSH key

Follow the instructions given [here](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/), and add the public key to the [dotfiles repo on bitbucket](https://bitbucket.org/bakken/dotfiles/src/master/).

## Clone the dotfiles repo and run install script from Git Bash as admin

1. `git clone git@bitbucket.org:bakken/dotfiles.git`
2. `source fresh_windows_install.sh`
3. Install fonts and icons manually as described [here](https://github.com/romkatv/powerlevel10k)

## Manual steps

1. Start `docker-desktop` and update drivers if necesarry.
2. Open settings in WindowsTerminal and replace Ubuntu-block with the content in dotfiles/settings.json. NB: Remember to change guid to the one given for Ubuntu BEFORE replacing.
3. Set up SSH key in wsl (se above)
4. Clone the repo `dotfiles` and run  `make all` inside the folder.
5. Install posh-git and oh-my-posh (this should be in a script...):
    - `Install-Module posh-git -Scope CurrentUser`
    - `Install-Module oh-my-posh -Scope CurrentUser`
    - `Set-Prompt`
    - `Set-Theme Paradox`
    - `notepad $PROFILE`
        - Append the following:
            - Import-Module posh-git
            - Import-Module oh-my-posh
            - Set-Theme Paradox
