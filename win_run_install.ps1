﻿choco install -y googlechrome 
choco install -y neovim
choco install -y python
choco install -y vscode
choco install -y vscode-docker
choco install -y insync
choco install -y git
choco install -y cmder
choco install -y foxitreader
choco install -y pdfcreator

$url = "https://github.com/randyrants/sharpkeys/releases/download/v3.9/sharpkeys39.msi"
$output = "$HOME\sharpekeys39.msi"
$start_time = Get-Date
Invoke-WebRequest -Uri $url -OutFile $output
Start-Process $HOME\sharpekeys39.msi
Write-Output "Time taken sharpekeys39: $((Get-Date).Subtract($start_time).Seconds) second(s)"