"let g:UltiSnipsListSnippets='<c-m>'
let g:UltiSnipsExpandTrigger = '<c-l>'
let g:UltiSnipsJumpForwardTrigger = '<c-j>'
let g:UltiSnipsJumpBackwardTrigger = '<c-k>'
let g:UltiSnipsRemoveSelectModeMappings = 0
"let g:UltiSnipsSnippetDirectories = [vimrc#path('UltiSnips')]

nnoremap <leader>es :UltiSnipsEdit!<cr>
