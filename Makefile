
all: misc nvim zsh oh-my-zsh powerlevel10k fzf cookiecutter poetry pyenv node fzf

misc:
	sudo apt-get update; sudo apt-get install --no-install-recommends make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
	sudo ln -s /usr/bin/python3 /usr/bin/python
nvim:
	@echo "Installing Neovim... \n****************\n" 
	curl -LO https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
	mkdir -p /home/bjorn/.appimages
	sudo mv nvim.appimage /home/bjorn/.appimages/
	sudo chmod +x /home/bjorn/.appimages/nvim.appimage
	sudo rm -f /usr/bin/nvim
	sudo ln -s /home/bjorn/.appimages/nvim.appimage /usr/bin/nvim
	@echo "Installing Python env for nvim... \n****************\n" 
	sudo apt-get install python3-venv -y
	python3 -m venv ~/nvim_venv
	~/nvim_venv/bin/pip3 install flake8 pyflakes pycodestyle neovim

zsh:
	@echo "Installing zsh... \n****************\n" 
	sudo apt install zsh -y
	chsh -s /usr/bin/zsh

oh-my-zsh:
	@echo "Installing oh-my-zsh... \n****************\n" 
	wget –no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O – | sh;
	wget --no-check-certificate http://install.ohmyz.sh -O - | sh;

powerlevel10k:
	@echo "Installing powerlevel10k... \n****************\n" 
	git clone https://github.com/romkatv/powerlevel10k.git /home/bjorn/.oh-my-zsh/custom/themes/powerlevel10k

fzf:
	@echo "Installing fzf... \n****************\n" 
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	~/.fzf/install

sourcecode:
	@echo "Cloning sourcecode... \n****************\n" 
	git clone git@bitbucket.org:rann/sourcecode.git ~/bitbucket/sourcecode

python3:
	@echo "Installing Python3.8... \n****************\n" 
	sudo add-apt-repository ppa:deadsnakes/ppa
	sudo apt update
	sudo apt install -y python3.8
	sudo apt install -y python3.8-venv
	sudo apt install -y python3-pip
	python3.8 -m venv ~/.venv


cookiecutter:
	python3.8 -m venv ~/.venv
	~/.venv/bin/python -m pip install cookiecutter

poetry:
	sudo curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3.8

pyenv:
	sudo curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

node:
	sudo curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh | bash
	sudo apt install -y nodejs
	sudo apt install -y npm

fzf:
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	~/.fzf/install

applications:
	mkdir -p ~/bitbucket/applications
	git clone git@bitbucket.org:rann/applications.git ~/bitbucket/applications
