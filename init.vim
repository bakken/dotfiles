""""""" Plugin management stuff
set nocompatible

if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')
    Plug 'KeitaNakamura/tex-conceal.vim', {'for': 'tex'}
    Plug 'benekastah/neomake'
    Plug 'morhetz/gruvbox'
    Plug 'neoclide/coc.nvim', {'branch': 'release', 'do':  { -> coc#util#install() }}
    "Plug 'neoclide/coc-denite'
    Plug 'thirtythreeforty/lessspace.vim'
    Plug 'mattn/emmet-vim'
    Plug 'tpope/vim-surround'
    Plug 'scrooloose/syntastic'
    Plug 'nvie/vim-flake8'
    Plug 'pangloss/vim-javascript'
    Plug 'leafgarland/typescript-vim'
    Plug 'bling/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'airblade/vim-gitgutter'
    Plug 'scrooloose/nerdcommenter'
    Plug 'junegunn/fzf'
    Plug 'junegunn/fzf.vim'
    Plug 'lervag/vimtex', {'tag': 'v1.6'}
    Plug 'jiangmiao/auto-pairs'
    Plug 'junegunn/limelight.vim'
    Plug 'reedes/vim-pencil'
    "Plug 'epilande/vim-react-snippets'
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'
    Plug 'chrisbra/Colorizer'
    Plug 'tpope/vim-commentary'
    Plug 'mxw/vim-jsx'
    Plug 'suy/vim-context-commentstring'
    Plug 'puremourning/vimspector'
    Plug 'ryanoasis/vim-devicons'
    "Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins'  }

    call plug#end()

set clipboard=unnamedplus
syntax on
set encoding=utf-8
set foldmethod=syntax
" Always show status bar
set laststatus=2
" Let plugins show effects after 500ms, not 4s
set updatetime=500
" Don't let autocomplete affect usual typing habits
set completeopt=menuone,preview,noinsert

set wrap linebreak
set tw=80

colorscheme gruvbox
"let g:gruvbox_contrast_dark = 'soft'
filetype plugin indent on

let g:airline_theme='distinguished'

set colorcolumn=80
highlight ColorColumn ctermbg=darkgrey

" Disable mouse click to go to position
"set mouse-=a
" Let vim-gitgutter do its thing on large files
let g:gitgutter_max_signs=10000


""""""" Keybindings """""""
" Set up leaders
let mapleader=","
let maplocalleader="\\"

" Vimspector
source ~/dotfiles/nvim/vimspector.vim
" Vimtex
source ~/dotfiles/nvim/vimtex.vim
"COC
source ~/dotfiles/nvim/coc.vim
"source ~/dotfiles/nvim/coc-snippets.vim
"
source ~/dotfiles/nvim/ultisnips.vim
"source ~/dotfiles/nvim/denite.vim

""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_general_viewer = 'zathura'


"autocmd FileType python nnoremap <buffer> <F9> :exec '!clear; python' shellescape(@%, 1)<cr>

"if has("multi_byte")
  "if &termencoding == ""
    "let &termencoding = &encoding
  "endif
  "set encoding=utf-8
  "setglobal fileencoding=utf-8
  ""setglobal bomb
  "set fileencodings=ucs-bom,utf-8,latin1
"endif


""""""" Jedi-VIM """""""
" Don't mess up undo history
"let g:jedi#show_call_signatures = "0"

""""""" python path for neovim """""""""
let g:python3_host_prog = '/home/bjorn/nvim_venv/bin/python'

" vimwiki
"let g:vimwiki_list = [{'path': '~/rann/bjorn/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
"let g:vimwiki_list = [{'path': '~/rann/bjorn/vimwiki/'}]

""""""" General coding stuff """""""
" Highlight 80th column

"let vim_markdown_preview_pandoc=1

"split navigations
"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

"Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

"command! MakeTags !ctags -R .

""""""" Python stuff """""""
"syntax enable
"set number showmatch
set shiftwidth=4 tabstop=4 softtabstop=4 expandtab autoindent
let python_highlight_all = 1

"let vim_markdown_preview_browser='Google Chrome'
"let g:mustache_abbreviations=1


inoremap <leader>, <C-x><C-o>

" Arrow keys up/down move visually up and down rather than by whole lines.  In
" other words, wrapped lines will take longer to scroll through, but better
" control in long bodies of text.
noremap <up> gk
noremap <down> gj

noremap <F2> :buffers<CR>:buffer<Space>

" Neomake and other build commands (ctrl-b)
nnoremap <C-b> :w<cr>:Neomake<cr>

" Emmet
let g:user_emmet_leader_key='<C-Z>'

autocmd BufNewFile,BufRead *.tex,*.bib noremap <buffer> <C-b> :w<cr>:new<bar>r !make<cr>:setlocal buftype=nofile<cr>:setlocal bufhidden=hide<cr>:setlocal noswapfile<cr>
autocmd BufNewFile,BufRead *.tex,*.bib imap <buffer> <C-b> <Esc><C-b>

 "FZM
set rtp+=/usr/local/opt/fzf
nnoremap <silent> <leader>f :FZF<cr>
nnoremap <silent> <leader>F :FZF ~<cr>


